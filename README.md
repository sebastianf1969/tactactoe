# tactactoe
tic tac toe demo using websockets and react frontend, completed games with wins are stored in postgresdb

## dependencies:
- postgres
    - postgres.sql file depends on a user named postgres to exist in postgres server
- nodejs
- react


## run instructions:
- import tactactoedb.sql file into postgres
    - use pgsql cmd interface and run:
    - \i <path/to/tactactodb.sql>
- update .env file with information related to your postgres setup
- run 'npm install' in 'server' directory
    - to start run: 'node wsGameServer.js' in 'server' directory
- run 'npm install' in 'tac-client' directory
    - to start run: 'npm start' in 'tac-client' directory

## playing the game:
- after running tac-client and node server:
    - open 2 browser instances 
    - play the game to completion
    - refresh to play again
- the game server only accepts 2 players to a single game instance a third browser will get disconnected from the game server.
- upon completion of the game (win, or tie) the gameserver will disconnect all players and reset its game instance.
    - upon a game win the server will upload the gamestate and winnerId (1 for player1 and 2 for player2) to postgres

## things to note:
- The gameserver does not handle restoring the state of a client who disconnects
    - As such, if a player disconnects mid game the game will not be able to continue until the rejoin, 
    - At which point the local state of that player will not be updated so its better to refresh and start a new game from both ends.
